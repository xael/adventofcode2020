# -*- coding: utf-8 -*-
"""Advent of code 2020, day 2

Usage:
   day2.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class PasswordDataConsumer:
    def __init__(self, filename):
        logging.debug(f"PasswordDataConsumer:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = []
            for line in data_file.read().strip().split("\n"):
                instructions, password = [x.strip() for x in line.split(":")]
                repeat, letter = instructions.split(" ")
                min_occurences, max_occurences = [int(x) for x in repeat.split("-")]
                self.data.append(
                    {
                        "min": min_occurences,
                        "max": max_occurences,
                        "password": password,
                        "letter": letter,
                    }
                )

    def read_data(self):
        """
        Return yield(data)
        """
        logging.debug("PasswordDataConsumer:read_data")
        for value in self.data:
            yield value


class PasswordTester(PasswordDataConsumer):
    def find_valid_passwords(self):
        valid_passwords = []
        for password_data in self.read_data():
            if self.is_valid(password_data):
                valid_passwords.append(password_data["password"])

        return valid_passwords

    @staticmethod
    def is_valid(data):
        occurences = len([x for x in data["password"] if x == data["letter"]])
        return data["min"] <= occurences and data["max"] >= occurences


class NewPasswordTester(PasswordTester):
    def __init__(self, filename):
        super().__init__(filename)
        new_data = []
        for data in self.data:
            data["pos1"] = data["min"] - 1
            del data["min"]
            data["pos2"] = data["max"] - 1
            del data["max"]
            new_data.append(data)

        self.data = new_data

    @staticmethod
    def is_valid(data):
        logging.debug(f"NewPasswordTester:is_valid {data}")
        test = (
            data["password"][data["pos1"]] == data["letter"]
            or data["password"][data["pos2"]] == data["letter"]
        ) and data["password"][data["pos1"]] != data["password"][data["pos2"]]
        logging.debug(f"{test}")
        return test


class PasswordTesterTestCase(unittest.TestCase):
    def test_example_part_1(self):
        pt = PasswordTester("data/test-data-day-2")
        assert pt.find_valid_passwords() == ["abcde", "ccccccccc"]

    def test_example_part_2(self):
        npt = NewPasswordTester("data/test-data-day-2")
        assert npt.find_valid_passwords() == ["abcde"]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")
        pt = PasswordTester("data/input-day-2")
        result = pt.find_valid_passwords()

        logging.info(f"Part 1 number of results found: {len(result)}")

        npt = NewPasswordTester("data/input-day-2")
        result = npt.find_valid_passwords()

        logging.info(f"Part 2 number of results found: {len(result)}")

# -*- coding: utf-8 -*-
"""Advent of code 2020, day 10

Usage:
   day10.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from functools import reduce

from docopt import docopt


class AdapterArray:
    def __init__(self, filename):
        logging.debug(f"AdapterArray:init:{filename}")
        self.adapters = []
        with open(filename, "r") as data_file:
            self.data = [
                self._format_item(x) for x in data_file.read().strip().split("\n")
            ]
            self.data.sort()

    @staticmethod
    def _format_item(item):
        return int(item)

    def count_adapters(self):
        adapters_in_bag = self.data
        jolt_level = 0
        adapters_list = {
            1: 0,
            2: 0,
            3: 1,
            "jolt_level": 0,
        }  # built in device add 3 jolt step
        while len(adapters_in_bag) > 0:
            selectable_adapter = self._next_allowed_adapter(jolt_level, adapters_in_bag)
            adapters_list[selectable_adapter - jolt_level] += 1
            jolt_level = selectable_adapter
            adapters_in_bag = self._remove_adapter_from_bag(
                selectable_adapter, adapters_in_bag
            )
            adapters_list["jolt_level"] = jolt_level

        return adapters_list

    def count_permutations(self):
        consecutives = self._count_consecutives_adapters()
        logging.debug(f"consecutive blocks: {consecutives}")
        permutations_counts = self._get_block_permutations(consecutives)
        logging.debug(f"permutations: {permutations_counts}")
        result = reduce((lambda x, y: x * y), permutations_counts)
        logging.debug(f"result: {result}")
        return result

    def _get_block_permutations(self, block):
        # because all blocks are separeted by at least 3 jolts
        # 2 consecutive blocks allows 1 valid combinaisons
        # 3 consecutive blocks allows 2 valid combinaisons
        # 123, 13
        # 4 consecutive blocks allows 4 valid combinaisons
        # 1234, 124, 134, 14
        # 5 consecutive blocks allows 6 valid combinaisons
        # 12345, 1235, 1245, 125, 1345, 135, 145
        perms = {
            5: 7,
            4: 4,
            3: 2,
            2: 1,
        }
        return [perms[x] for x in block]

    def _count_consecutives_adapters(self):
        blocks = []
        current_block = []
        all_adapters = [0] + self.data + [self.data[-1] + 4]
        for index in range(len(all_adapters) - 1):
            adapter = all_adapters[index]
            next_adapter = all_adapters[index + 1]
            if adapter + 1 == next_adapter:
                current_block.append(adapter)
            else:
                if len(current_block) != 0:
                    current_block.append(adapter)
                    blocks.append(current_block)
                    current_block = []

        if len(current_block) != 0:
            blocks.append(current_block)

        logging.debug(f"blocks: {blocks}")
        return [len(x) for x in blocks]

    def _remove_adapter_from_bag(self, adapter, adapters_in_bag):
        return [x for x in adapters_in_bag if x > adapter]

    def _next_allowed_adapter(self, jolt_level, adapters_in_bag):
        return min(self._next_possible_adapters(jolt_level, adapters_in_bag))

    def _next_possible_adapters(self, jolt_level, adapters_in_bag):
        selectable = [
            x for x in adapters_in_bag if x >= jolt_level + 1 and x <= jolt_level + 3
        ]
        if len(selectable) == 0:
            raise ValueError("Non applicable jolt adapter")
        return selectable


class AdapterArrayTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = AdapterArray("data/test-data-day-10")
        adapters = runner.count_adapters()
        logging.debug(adapters)
        logging.info(
            f"Part 1 result: {adapters[1] * adapters[3]} - jolt_level: {adapters['jolt_level']} "
        )
        assert adapters[1] == 22
        assert adapters[3] == 10

    def test_example_part_2_small(self):
        runner = AdapterArray("data/test-data-day-10-part-2")
        nb = runner.count_permutations()
        logging.debug(f"NB:{nb}")
        assert nb == 8

    def test_example_part_2(self):
        runner = AdapterArray("data/test-data-day-10")
        nb = runner.count_permutations()
        logging.debug(f"NB:{nb}")
        assert nb == 19208


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = AdapterArray("data/input-day-10")
        adapters = runner.count_adapters()
        logging.info(
            f"Part 1 1:{adapters[1]} 2:{adapters[2]} 3:{adapters[3]} "
            f"result: {adapters[1] * adapters[3]} - jolt_level: {adapters['jolt_level']} "
        )

        logging.info("Part 2")
        runner = AdapterArray("data/input-day-10")
        nb = runner.count_permutations()
        logging.info(f"Part 2 nb: {nb}  ")

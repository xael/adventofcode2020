# -*- coding: utf-8 -*-
"""Advent of code 2020, day 16

Usage:
   day16.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""

import copy
import logging
import re
import sys
import unittest
from itertools import accumulate

from docopt import docopt


class TicketTranslation:
    def __init__(self, filename):
        logging.debug(f"TicketTranslation:init:{filename}")
        with open(filename, "r") as data_file:
            data_blocks = data_file.read().strip().split("\n\n")
            self._read_rules(data_blocks[0])
            self._read_my_ticket(data_blocks[1])
            self._read_nearby_tickets(data_blocks[2])

    def _read_rules(self, data):
        rule_regex = re.compile(
            r"(?P<name>[\w\ ]+):\ (?P<rule1_l>\d*)-(?P<rule1_h>\d*)\ or\ (?P<rule2_l>\d*)-(?P<rule2_h>\d*)"
        )
        self.rules = {}
        for line in data.split("\n"):
            match = rule_regex.match(line)
            if match is None:
                logging.error(f"error with line: {line}")
            if match is not None:
                values = match.groupdict()
                self.rules[values["name"]] = {
                    "rule1_l": int(values["rule1_l"]),
                    "rule1_h": int(values["rule1_h"]),
                    "rule2_l": int(values["rule2_l"]),
                    "rule2_h": int(values["rule2_h"]),
                }
        logging.debug(f"_read_rules: {self.rules}")

    def _read_my_ticket(self, data):
        pass

    def _read_nearby_tickets(self, data):
        self.nearby_tickets = [
            [int(z) for z in y] for y in [x.split(",") for x in data.split("\n")[1:]]
        ]
        logging.debug(f"_read_nearby_tickets: {self.nearby_tickets}")

    def run(self):
        invalid_values = []
        for ticket in self.nearby_tickets:
            valid, errors = self._check_ticket_validity(ticket)
            if not valid:
                invalid_values.append(errors)

        return invalid_values

    def _check_ticket_validity(self, ticket):
        fields = {}
        anormal_fields = []
        for field in ticket:
            rules = self._field_match_these_rules(field)
            if rules is not None:
                fields[field] = rules
            else:
                anormal_fields.append(field)

        return len(anormal_fields) == 0, anormal_fields

    def _field_match_these_rules(self, field):
        matching_rules = []
        for r in self.rules:
            if (
                self.rules[r]["rule1_l"] <= field and field <= self.rules[r]["rule1_h"]
            ) or (
                self.rules[r]["rule2_l"] <= field and field <= self.rules[r]["rule2_h"]
            ):
                matching_rules.append(r)

        if len(matching_rules) == 0:
            return None

        return matching_rules


class TicketTranslationGuessField(TicketTranslation):
    def __init__(self, filename):
        super().__init__(filename)
        self.ordered_fields = []

    def _read_my_ticket(self, data):
        self.my_ticket = [
            [int(z) for z in y] for y in [x.split(",") for x in data.split("\n")[1:]]
        ][0]
        logging.debug(f"_read_my_ticket: {self.my_ticket}")

    def run(self):
        fields = self._guess_fields_order()
        values = 1
        for i in range(len(fields)):
            if fields[i].startswith("departure"):
                values = values * self.my_ticket[i]

        return values

    def _guess_fields_order(self):
        ordered_fields = []
        first_run = True
        for ticket in self.nearby_tickets:
            valid, errors = self._check_ticket_validity(ticket)
            if valid:
                ticket_fields = self._guess_fields_for_ticket(ticket)
                if first_run:
                    first_run = False
                    ordered_fields = copy.deepcopy(ticket_fields)
                else:
                    new_ordered_fields = []
                    for index in range(len(ordered_fields)):
                        new_fields = []
                        for field in ordered_fields[index]:
                            if field in ticket_fields[index]:
                                new_fields.append(field)

                        new_ordered_fields.append(new_fields)

                    ordered_fields = copy.deepcopy(new_ordered_fields)

                logging.debug(f"ordered_fields: {ordered_fields}")

        ordered_fields = self._reduce_rules(ordered_fields)
        logging.debug(f"Reduced ordered_fields: {ordered_fields}")
        return ordered_fields

    @staticmethod
    def _reduce_rules(rules):
        logging.debug(f"_reduce_rules: {rules}")
        new_rules = copy.deepcopy(rules)
        lengths = [len(x) for x in rules]
        if len([x for x in lengths if x != 1]) == 0:
            return [x[0] for x in rules]
        for i in range(len(lengths)):
            if lengths[i] == 1:
                unique_rule = new_rules[i][0]
                # remove unique rule from others
                for j in range(len(lengths)):
                    if i == j:
                        continue

                    new_rules[j] = [x for x in new_rules[j] if x != unique_rule]

        return TicketTranslationGuessField._reduce_rules(new_rules)

    def _guess_fields_for_ticket(self, ticket):
        fields = []
        for field in ticket:
            rules = self._field_match_these_rules(field)
            fields.append(rules)

        return fields


class TicketTranslationTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = TicketTranslation("data/test-data-day-16")
        result = runner.run()
        error_rate = 0
        for error in result:
            score = list(accumulate(error))[-1]
            error_rate += score

        logging.info(f"Test Part 1: result: {result} error_rate: {error_rate}")
        assert error_rate == 71

    def test_example_part_2(self):
        runner = TicketTranslationGuessField("data/test-data-day-16-part-2")
        result = runner._guess_fields_order()
        logging.info(f"Test Part 2: {result}")
        assert result == ["row", "class", "seat"]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = TicketTranslation("data/input-day-16")
        result = runner.run()
        error_rate = 0
        for error in result:
            score = list(accumulate(error))[-1]
            error_rate += score

        logging.info(f"Test Part 1: result: {result} error_rate: {error_rate}")
        logging.info(f"Part 1: {error_rate}")

        logging.info("Part 2")
        runner = TicketTranslationGuessField("data/input-day-16")
        result = runner.run()
        logging.info(f"Part 2: {result}")

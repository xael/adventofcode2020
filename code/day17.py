# -*- coding: utf-8 -*-
"""Advent of code 2020, day 17

Usage:
   day17.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class ConwayCube:
    ACTIVE = "#"
    INACTIVE = "."

    def __init__(self, filename):
        self.cycles = 0
        with open(filename, "r") as data_file:
            self.lines = data_file.read().strip().split("\n")
            self.cube = {}
            z = 0
            for y in range(len(self.lines)):
                for x in range(len(self.lines[y])):
                    self.set_at(x, y, z, self.lines[y][x])

    def set_at(self, x, y, z, value):
        self.cube[(x, y, z)] = value

    def get_at(self, x, y, z):
        return self.cube[(x, y, z)] if (x, y, z) in self.cube else ConwayCube.INACTIVE

    @property
    def min_max(self):
        keys = self.cube.keys()
        x_min = min(d[0] for d in keys)
        x_max = max(d[0] for d in keys)
        y_min = min(d[1] for d in keys)
        y_max = max(d[1] for d in keys)
        z_min = min(d[2] for d in keys)
        z_max = max(d[2] for d in keys)

        return {
            "x": (x_min, x_max),
            "y": (y_min, y_max),
            "z": (z_min, z_max),
        }

    def display_cube(self):
        min_max = self.min_max
        out = f"\n* Cube (cycle: {self.cycles}) *\n"
        for z in range(min_max["z"][0], min_max["z"][1] + 1):
            out += f"\nZ-level: {z}\n"
            for y in range(min_max["y"][0], min_max["y"][1] + 1):
                for x in range(min_max["x"][0], min_max["x"][1] + 1):
                    out += self.get_at(x, y, z)

                out += "\n"

        logging.info(out)

    def get_neighbors_state(self, x, y, z):
        states = {
            ConwayCube.ACTIVE: 0,
            ConwayCube.INACTIVE: 0,
        }
        for dx, dy, dz in [
            (x, y, z)
            for z in [-1, 0, 1]
            for y in [-1, 0, 1]
            for x in [-1, 0, 1]
            if (x, y, z) != (0, 0, 0)
        ]:
            value = self.get_at(x + dx, y + dy, z + dz)
            states[value] += 1

        return states

    def run(self, cycles):
        for _ in range(cycles):
            self.define_next_state()
            self.display_cube()

    def count_active_cubes(self):
        actives = 0
        min_max = self.min_max
        for z in range(min_max["z"][0], min_max["z"][1] + 1):
            for y in range(min_max["y"][0], min_max["y"][1] + 1):
                for x in range(min_max["x"][0], min_max["x"][1] + 1):
                    if self.get_at(x, y, z) == ConwayCube.ACTIVE:
                        actives += 1

        return actives

    def define_next_state(self):
        to_active = []
        to_inactive = []
        min_max = self.min_max
        for z in range(min_max["z"][0] - 1, min_max["z"][1] + 2):
            for y in range(min_max["y"][0] - 1, min_max["y"][1] + 2):
                for x in range(min_max["x"][0] - 1, min_max["x"][1] + 2):
                    cell_state = self.get_at(x, y, z)
                    state = self.get_neighbors_state(x, y, z)
                    if cell_state == ConwayCube.ACTIVE and state[
                        ConwayCube.ACTIVE
                    ] not in (2, 3):
                        to_inactive.append((x, y, z))
                    elif (
                        cell_state == ConwayCube.INACTIVE
                        and state[ConwayCube.ACTIVE] == 3
                    ):
                        to_active.append((x, y, z))

        for cell in to_active:
            self.set_at(cell[0], cell[1], cell[2], ConwayCube.ACTIVE)

        for cell in to_inactive:
            self.set_at(cell[0], cell[1], cell[2], ConwayCube.INACTIVE)

        self.cycles += 1
        return


class ConwayHyperCube:
    ACTIVE = "#"
    INACTIVE = "."

    def __init__(self, filename):
        self.cycles = 0
        with open(filename, "r") as data_file:
            self.lines = data_file.read().strip().split("\n")
            self.cube = {}
            z = 0
            w = 0
            for y in range(len(self.lines)):
                for x in range(len(self.lines[y])):
                    self.set_at(x, y, z, w, self.lines[y][x])

    def set_at(self, x, y, z, w, value):
        self.cube[(x, y, z, w)] = value

    def get_at(self, x, y, z, w):
        return (
            self.cube[(x, y, z, w)]
            if (x, y, z, w) in self.cube
            else ConwayHyperCube.INACTIVE
        )

    @property
    def min_max(self):
        keys = self.cube.keys()
        x_min = min(d[0] for d in keys)
        x_max = max(d[0] for d in keys)
        y_min = min(d[1] for d in keys)
        y_max = max(d[1] for d in keys)
        z_min = min(d[2] for d in keys)
        z_max = max(d[2] for d in keys)
        w_min = min(d[3] for d in keys)
        w_max = max(d[3] for d in keys)

        return {
            "x": (x_min, x_max),
            "y": (y_min, y_max),
            "z": (z_min, z_max),
            "w": (w_min, w_max),
        }

    def display_cube(self):
        min_max = self.min_max
        out = f"\n* Cube (cycle: {self.cycles}) *\n"
        for w in range(min_max["w"][0], min_max["w"][1] + 1):
            for z in range(min_max["z"][0], min_max["z"][1] + 1):
                out += f"\nLevel: z:{z} w:{w}\n"
                for y in range(min_max["y"][0], min_max["y"][1] + 1):
                    for x in range(min_max["x"][0], min_max["x"][1] + 1):
                        out += self.get_at(x, y, z, w)

                    out += "\n"

        logging.info(out)

    def get_neighbors_state(self, x, y, z, w):
        states = {
            ConwayHyperCube.ACTIVE: 0,
            ConwayHyperCube.INACTIVE: 0,
        }
        for dx, dy, dz, dw in [
            (x, y, z, w)
            for w in [-1, 0, 1]
            for z in [-1, 0, 1]
            for y in [-1, 0, 1]
            for x in [-1, 0, 1]
            if (x, y, z, w) != (0, 0, 0, 0)
        ]:
            value = self.get_at(x + dx, y + dy, z + dz, w + dw)
            states[value] += 1

        return states

    def run(self, cycles):
        for _ in range(cycles):
            self.define_next_state()
            self.display_cube()

    def count_active_cubes(self):
        actives = 0
        min_max = self.min_max
        for w in range(min_max["w"][0], min_max["w"][1] + 1):
            for z in range(min_max["z"][0], min_max["z"][1] + 1):
                for y in range(min_max["y"][0], min_max["y"][1] + 1):
                    for x in range(min_max["x"][0], min_max["x"][1] + 1):
                        if self.get_at(x, y, z, w) == ConwayHyperCube.ACTIVE:
                            actives += 1

        return actives

    def define_next_state(self):
        to_active = []
        to_inactive = []
        min_max = self.min_max
        for w in range(min_max["w"][0] - 1, min_max["w"][1] + 2):
            for z in range(min_max["z"][0] - 1, min_max["z"][1] + 2):
                for y in range(min_max["y"][0] - 1, min_max["y"][1] + 2):
                    for x in range(min_max["x"][0] - 1, min_max["x"][1] + 2):
                        cell_state = self.get_at(x, y, z, w)
                        state = self.get_neighbors_state(x, y, z, w)
                        if cell_state == ConwayHyperCube.ACTIVE and state[
                            ConwayHyperCube.ACTIVE
                        ] not in (2, 3):
                            to_inactive.append((x, y, z, w))
                        elif (
                            cell_state == ConwayHyperCube.INACTIVE
                            and state[ConwayHyperCube.ACTIVE] == 3
                        ):
                            to_active.append((x, y, z, w))

        for cell in to_active:
            self.set_at(cell[0], cell[1], cell[2], cell[3], ConwayHyperCube.ACTIVE)

        for cell in to_inactive:
            self.set_at(cell[0], cell[1], cell[2], cell[3], ConwayHyperCube.INACTIVE)

        self.cycles += 1
        return


class ConwayCubeTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = ConwayCube("data/test-data-day-17")
        runner.run(cycles=6)
        result = runner.count_active_cubes()
        logging.info(f"Test Part 1: {result}")
        assert result == 112

    def test_example_part_2(self):
        runner = ConwayHyperCube("data/test-data-day-17")
        runner.display_cube()
        runner.run(cycles=6)
        result = runner.count_active_cubes()
        logging.info(f"Test Part 2: {result}")
        assert result == 848


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = ConwayCube("data/input-day-17")
        runner.display_cube()
        runner.run(cycles=6)
        result = runner.count_active_cubes()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = ConwayHyperCube("data/input-day-17")
        runner.display_cube()
        runner.run(cycles=6)
        result = runner.count_active_cubes()
        logging.info(f"Part 2: {result}")

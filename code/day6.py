# -*- coding: utf-8 -*-
"""Advent of code 2020, day 6

Usage:
   day6.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class CustomDeclaration:
    def __init__(self, filename):
        logging.debug(f"CustomDeclaration:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n\n")

    @property
    def declarations(self):
        for declaration in self.data:
            yield self._format_declaration(declaration)

    @staticmethod
    def _format_declaration(declaration):
        return declaration.replace("\n", "")

    def count_yes(self):
        count = 0
        for d in self.declarations:
            count += len(set(d))

        return count


class CustomDeclarationAllHaveAnswerYes(CustomDeclaration):
    @staticmethod
    def _format_declaration(declaration):
        return declaration

    def count_yes(self):
        count = 0
        for declaration_set in self.declarations:
            result = None
            for declaration in declaration_set.split("\n"):
                if result is None:
                    result = set(declaration)
                else:
                    result = result.intersection(set(declaration))

            count += len(result)
        return count


class CustomDeclarationTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = CustomDeclaration("data/test-data-day-6")
        count = runner.count_yes()
        assert count == 11

    def test_example_part_2(self):
        runner = CustomDeclarationAllHaveAnswerYes("data/test-data-day-6")
        count = runner.count_yes()
        assert count == 6


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = CustomDeclaration("data/input-day-6")
        count = runner.count_yes()
        logging.info(f"Sum of yes : {count}")

        logging.info("Part 2")
        runner = CustomDeclarationAllHaveAnswerYes("data/input-day-6")
        count = runner.count_yes()
        logging.info(f"Sum of yes : {count}")

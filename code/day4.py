# -*- coding: utf-8 -*-
"""Advent of code 2020, day 4

Usage:
   day4.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import re
import sys
import unittest

from docopt import docopt


class DocumentReader:
    def __init__(self, filename):
        logging.debug(f"DocumentReader:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n\n")
        logging.debug(f"DocumentReader:init:got {len(self.data)} passports")

    @property
    def documents(self):
        for document in self.data:
            yield self.format_document(document)

    def format_document(self, document):
        # put all field on one line ordered by alphabetical order
        fields = document.replace("\n", " ").split(" ")
        fields.sort()
        return " ".join(fields)


class DocumentScanner:
    _valid_north_pole_credential = re.compile(
        r"""
byr:(?P<byr>\w+)\ .*
ecl:(?P<ecl>\w+)\ .*
eyr:(?P<eyr>\w+)\ .*
hcl:(?P<hcl>\#\w+)\ .*
hgt:(?P<hgt>\w+)\ .*
iyr:(?P<iyr>\w+)\ .*
pid:(?P<pid>\w+)
""",
        re.VERBOSE | re.IGNORECASE,
    )
    _valid_passport = re.compile(
        r"""
byr:(?P<byr>\w+)\ .*
cid:(?P<cid>\w+)\ .*
ecl:(?P<ecl>\w+)\ .*
eyr:(?P<eyr>\w+)\ .*
hcl:(?P<hcl>\#\w+)\ .*
hgt:(?P<hgt>\w+)\ .*
iyr:(?P<iyr>\w+)\ .*
pid:(?P<pid>\w+)
""",
        re.VERBOSE | re.IGNORECASE,
    )

    def __init__(self, filename):
        self.document_reader = DocumentReader(filename)

    def parse_documents(self):
        valid_passports = 0
        for document in self.document_reader.documents:
            is_valid = self._is_valid_document(document)
            logging.debug(f"DocumentScanner:parsing: {document} is {is_valid}")
            if is_valid:
                valid_passports += 1

        return valid_passports

    @classmethod
    def _is_valid_document(cls, document):
        return cls._is_valid_passport(document) or cls._is_valid_north_pole_credentials(
            document
        )

    @classmethod
    def _is_valid_north_pole_credentials(cls, document):
        match = cls._valid_north_pole_credential.match(document)
        if match is not None:
            logging.debug(match.groupdict())
        return match is not None

    @classmethod
    def _is_valid_passport(cls, document):
        match = cls._valid_passport.match(document)
        if match is not None:
            logging.debug(match.groupdict())
        return match is not None


class SecureDocumentScanner(DocumentScanner):
    _valid_document = re.compile(
        r"""
byr:(?P<byr>(19[2-9]\d|200[0-2]))\ .*                         # (Birth Year) - four digits; at least 1920 and at most 2002.
ecl:(?P<ecl>(amb|blu|brn|gry|grn|hzl|oth))\ .*                # (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
eyr:(?P<eyr>20(2\d|30))\ .*                                   # (Expiration Year) - four digits; at least 2020 and at most 2030.
hcl:\#(?P<hcl>[0-9a-f]{6})\ .*                                # (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
hgt:(?P<hgt>(1([5-8][0-9]|9[0-3])cm|(59|6\d|7[0-6])in))\ .*   # (Height) - a number followed by either cm or in:
                                                              #    If cm, the number must be at least 150 and at most 193.
                                                              #    If in, the number must be at least 59 and at most 76.
iyr:(?P<iyr>20(1\d|20))\ .*                                   # (Issue Year) - four digits; at least 2010 and at most 2020.
pid:(?P<pid>\d{9})(\ |$)                                            # (Passport ID) - a nine-digit number, including leading zeroes.
""",  # NOQA: E501
        re.VERBOSE | re.IGNORECASE,
    )

    @classmethod
    def _is_valid_document(cls, document):
        return cls._is_valid_values(document)

    def parse_valid_documents(self):
        valid_passports = 0
        for document in self.document_reader.documents:
            is_valid = self._is_valid_document(document)
            logging.debug(f"SecureDocumentScanner:parsing: {document} is {is_valid}")
            if is_valid:
                valid_passports += 1

        return valid_passports

    @classmethod
    def _is_valid_values(cls, document):
        match = cls._valid_document.match(document)
        if match is not None:
            data = match.groupdict()
            logging.debug(f"Matching {data}")
            return True

        return False


class DocumentScannerTestCase(unittest.TestCase):
    def test_example_part_1(self):
        logging.debug("----------------  Run test 1")
        runner = DocumentScanner("data/test-data-day-4")
        valid_passports = runner.parse_documents()
        assert valid_passports == 2

    def test_example_part_2(self):
        logging.debug("----------------  Run test 2.1")
        runner = SecureDocumentScanner("data/test-data-day-4-invalid-passports")
        valid_passports = runner.parse_documents()
        assert valid_passports == 0

        logging.debug("----------------  Run test 2.2")
        runner = SecureDocumentScanner("data/test-data-day-4-valid-passports")
        valid_passports = runner.parse_documents()
        assert valid_passports == 4


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = DocumentScanner("data/input-day-4")
        valid_passports = runner.parse_documents()

        logging.info(f"Got {valid_passports} valid passports")

        logging.info("Part 2")
        runner = SecureDocumentScanner("data/input-day-4")
        valid_passports = runner.parse_valid_documents()

        logging.info(f"Got {valid_passports} valid passports")

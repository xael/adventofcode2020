# -*- coding: utf-8 -*-
"""Advent of code 2020, day 11

Usage:
   day11.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""

import copy
import logging
import sys
import unittest

from docopt import docopt


FLOOR = "."
EMPTY = "L"
OCCUPIED = "#"


class SeatingSystem:
    def __init__(self, filename):
        logging.debug(f"SeatingSystem:init:{filename}")
        with open(filename, "r") as data_file:

            self.data = [list(y) for y in data_file.read().strip().split("\n")]
            self.x_size = len(self.data[0])
            self.y_size = len(self.data)

    def _get_neighbors_at_pos(self, x, y):
        state = {
            EMPTY: 0,
            OCCUPIED: 0,
            FLOOR: 0,
        }
        for dx, dy in [
            (x, y) for y in [-1, 0, 1] for x in [-1, 0, 1] if (x, y) != (0, 0)
        ]:
            status = self._get_seat_at_pos(x + dx, y + dy)
            state[self._get_seat_at_pos(x + dx, y + dy)] += 1
            logging.debug(f"pos {x + dx:2},{y + dy:2} {status}")
        return state

    def _get_seat_at_pos(self, x, y):
        if x == -1 or y == -1 or x > self.x_size - 1 or y > self.y_size - 1:
            return FLOOR  # Consider walls as floor

        return self.data[y][x]

    def _generate_next_occupation(self):
        plan = copy.deepcopy(self.data)
        for x, y in [(x, y) for y in range(self.y_size) for x in range(self.x_size)]:
            current_seat = self._get_seat_at_pos(x, y)
            neighbors = self._get_neighbors_at_pos(x, y)
            logging.debug(f"pos {x},{y} seat:{current_seat} neighbors:{neighbors}")
            if current_seat == EMPTY and neighbors[OCCUPIED] == 0:
                plan[y][x] = OCCUPIED
            elif current_seat == OCCUPIED and self._is_too_crowded(
                occupation_rate=neighbors[OCCUPIED]
            ):
                plan[y][x] = EMPTY

        self._print_plan(plan)
        return plan

    @staticmethod
    def _is_too_crowded(occupation_rate):
        return occupation_rate >= 4

    def _print_plan(self, plan):
        data = "\n"
        for line in plan:
            for cell in line:
                data += cell
            data += "\n"
        logging.info(data)

    def run_generator_until_stabilization(self):
        plan = [[]]
        generation = 0

        while not self._compare_plans(plan, self.data):
            logging.info(f"Generation: {generation}")
            new_plan = self._generate_next_occupation()
            generation += 1
            plan = copy.deepcopy(self.data)
            self.data = copy.deepcopy(new_plan)

        state = self._count_occupation()
        return state

    @staticmethod
    def _compare_plans(plan1, plan2):
        string1 = "\n".join(["".join(list(y)) for y in plan1])
        string2 = "\n".join(["".join(list(y)) for y in plan2])
        return string1 == string2

    def _count_occupation(self):
        state = {
            EMPTY: 0,
            OCCUPIED: 0,
            FLOOR: 0,
        }
        for x, y in [(x, y) for y in range(self.y_size) for x in range(self.x_size)]:
            state[self._get_seat_at_pos(x, y)] += 1

        return state


class NewSeatingSystem(SeatingSystem):
    def _get_neighbors_at_pos(self, x, y):
        state = {
            EMPTY: 0,
            OCCUPIED: 0,
            FLOOR: 0,
        }
        for dx, dy in [
            (x, y) for y in [-1, 0, 1] for x in [-1, 0, 1] if (x, y) != (0, 0)
        ]:
            state_in_direction = self._walk_on_board_until_occupied_or_empty(
                x, y, dx, dy
            )
            state[state_in_direction] += 1

        return state

    def _walk_on_board_until_occupied_or_empty(self, x, y, dx, dy):
        cx, cy = x + dx, y + dy
        while self._is_on_board(cx, cy):
            cstate = self._get_seat_at_pos(cx, cy)
            if cstate == OCCUPIED:
                return OCCUPIED
            elif cstate == EMPTY:
                return EMPTY

            cx += dx
            cy += dy

        return FLOOR

    def _is_on_board(self, x, y):
        return x >= 0 and x < self.x_size and y >= 0 and y < self.y_size

    @staticmethod
    def _is_too_crowded(occupation_rate):
        return occupation_rate >= 5


class SeatingSystemTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = SeatingSystem("data/test-data-day-11")
        nb = runner.run_generator_until_stabilization()
        logging.info(f"Part 1 : {nb}")
        assert nb[OCCUPIED] == 37

    def test_example_part_2(self):
        runner = NewSeatingSystem("data/test-data-day-11")
        nb = runner.run_generator_until_stabilization()
        logging.info(f"Part 2 : {nb}")
        assert nb[OCCUPIED] == 26


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = SeatingSystem("data/input-day-11")
        nb = runner.run_generator_until_stabilization()
        logging.info(f"Part 1 : {nb}")

        logging.info("Part 2")
        runner = NewSeatingSystem("data/input-day-11")
        nb = runner.run_generator_until_stabilization()
        logging.info(f"Part 2 : {nb}")

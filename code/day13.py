# -*- coding: utf-8 -*-
"""Advent of code 2020, day 13

Usage:
   day13.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""

import logging
import sys
import unittest
from functools import reduce

from docopt import docopt


class ShuttleSearch:
    def __init__(self, filename):
        logging.debug(f"ShuttleSearch:init:{filename}")
        with open(filename, "r") as data_file:
            current_time, lines = data_file.read().strip().split("\n")
            self.current_time = int(current_time)
            self.lines = [int(x) for x in lines.split(",") if x != "x"]

    def find_first(self):
        time = self.current_time
        while True:
            logging.debug(f"Time: {time}")
            for line in self.lines:
                if time % line == 0:
                    logging.debug(f"Found: time:{time}, line:{line}")
                    return time - self.current_time, line

            time += 1


class ShuttleSearchContest:
    def __init__(self, filename):
        logging.debug(f"ShuttleSearchContest:init:{filename}")
        with open(filename, "r") as data_file:
            lines = data_file.read().strip().split("\n")[1]
            self.lines = []
            for line in lines.split(","):
                self.lines.append(int(line) if line != "x" else line)

    def iterate_over_max_id(self):
        max_id = max(x for x in self.lines if x != "x")
        index_max_id = self.lines.index(max_id)
        iteration = 0
        while True:
            if iteration % max_id + index_max_id == max_id:
                logging.info(f"iteration:{iteration}")
                yield iteration

            iteration += 1

    def find_first_occurence_brute_force(self):
        time = 0
        for time in self.iterate_over_max_id():
            logging.debug(f"time: {time}")
            found = True
            for index in range(len(self.lines)):
                if self.lines[index] == "x":
                    continue

                reste = time % self.lines[index]
                logging.debug(
                    f"time:{time} line:{self.lines[index]} index:{index} R:{reste} R+I={reste+index}"
                )
                if (
                    not (index == 0 and reste == 0)
                    and reste + index != self.lines[index]
                ):
                    found = False
                    break

            if found:
                return time

    def find_first_occurence(self):
        # https://fr.m.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_des_restes_chinois
        data = []
        for i in range(len(self.lines)):
            if self.lines[i] != "x":
                data.append((self.lines[i], i, self.lines[i] - i % self.lines[i]))

        logging.info(f"Chinese theorem data\n{data}")
        n = reduce((lambda x, y: x * y), [x[0] for x in data])
        solution = 0
        for i in data:
            np = reduce((lambda x, y: x * y), [x[0] for x in data if x[0] != i[0]])
            logging.info(f"x === {i[2]} (mod {i[0]}) => n: {n} / np: {np}")
            factor = 1
            while 1 != (np * factor) % i[0]:
                factor += 1

            logging.info(
                f"factor = {factor} : {np} * {factor} % {i[0]} = {(np * factor) % i[0]}"
            )
            e = np * factor * i[2]
            logging.info(f"solution += {i[2]} * {factor} * {np} --  {e}")
            solution += e

        solution = solution % n
        return solution


class ShuttleSearchTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = ShuttleSearch("data/test-data-day-13")
        first_time_and_id = runner.find_first()
        assert first_time_and_id[0] * first_time_and_id[1] == 295

    def test_example_part_2(self):
        runner = ShuttleSearchContest("data/test-data-day-13")
        time = runner.find_first_occurence()
        logging.info(f"Part 2 {time}")
        assert time == 1068781


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = ShuttleSearch("data/input-day-13")
        first_time_and_id = runner.find_first()
        logging.info(
            f"Part 1 : time to wait: {first_time_and_id[0]} line: {first_time_and_id[1]} "
            f"result: {first_time_and_id[0] * first_time_and_id[1]}"
        )

        logging.info("Part 2")
        runner = ShuttleSearchContest("data/input-day-13")
        time = runner.find_first_occurence()
        logging.info(f"Part 2 : time: {time}")

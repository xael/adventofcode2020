# -*- coding: utf-8 -*-
"""Advent of code 2020, day 18

Usage:
   day18.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import re
import sys
import unittest

from docopt import docopt


class OperationOrder:
    def __init__(self, homework):
        logging.debug(f"OperationOrder:init:{homework}")
        self.homework = homework.replace(" ", "")

    def evaluate(self):
        result = int(self._analyse(self.homework))
        return result

    @staticmethod
    def _analyse(homework):
        logging.debug(f"_analyse {str(homework)}")
        regex = re.compile(r"\((?P<func>[\d+\*]*?)\)")
        while True:
            match = regex.search(homework)
            if match is None:
                break

            group = match.group()
            values = match.groupdict()
            group_result = OperationOrder._analyse(values["func"])
            homework = homework.replace(group, str(group_result))
            logging.debug(
                f"Found group {group} replaced as {group_result} -> {homework}"
            )

        regex = re.compile(r"(?P<d1>\d*)(?P<op>[*|+])(?P<d2>\d*)")
        while True:
            match = regex.search(homework)
            if match is None:
                break

            values = match.groupdict()
            group = match.group()
            d1 = values["d1"]
            d2 = values["d2"]
            op = values["op"]
            if op == "+":
                result = int(d1) + int(d2)
                homework = homework.replace(group, str(result))
            elif op == "*":
                result = int(d1) * int(d2)
                homework = homework.replace(group, str(result))

            homework = homework.replace(group, str(result))
            logging.debug(f"Found group {group} replaced as {result}")

        return homework


class OperationOrderTestCase(unittest.TestCase):
    def test_example_part_1(self):
        data = (
            ("2 * 3 + (4 * 5)", 26),
            ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437),
            ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240),
            ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632),
            ("1 + (2 * 3) + (4 * (5 + 6))", 51),
            ("1 + 2 * 3 + 4 * 5 + 6", 71),
        )
        for item in data:
            runner = OperationOrder(item[0])
            result = runner.evaluate()
            logging.info(f"Test Part 1: {item[0]} -> {result} on {item[1]}")
            assert result == item[1]

    # def test_example_part_2(self):
    #     runner = OperationOrder("data/test-data-day-18")
    #     result = runner.evaluate()
    #     logging.info(f"Test Part 2: {result}")
    #     assert False


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        sum_of_values = 0
        with open("data/input-day-18", "r") as data_file:
            for line in data_file.read().strip().split("\n"):
                runner = OperationOrder(line)
                result = runner.evaluate()
                logging.info(f"Part 1: {line} = {result}")
                sum_of_values += result

        logging.info(f"Part 1: {sum_of_values}")

        logging.info("Part 2")
        runner = OperationOrder("data/input-day-18")
        result = runner.evaluate()
        logging.info(f"Part 2: {result}")

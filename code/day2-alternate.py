# -*- coding: utf-8 -*-
"""Advent of code 2020, day 2

Usage:
   day2.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import re
import sys
import unittest

from docopt import docopt


class PasswordDataConsumer:
    match_to_int = ["min", "max"]

    def __init__(self, filename):
        logging.debug(f"PasswordDataConsumer:init:{filename}")
        with open(filename, "r") as data_file:
            pattern = self._matching_regex
            self.data = []
            for line in data_file.read().strip().split("\n"):
                match = pattern.match(line)
                if match is None:
                    logging.warning(f"PasswordDataConsumer:init:Ignored line {line}")
                    continue
                values = match.groupdict()
                for key in self.match_to_int:
                    values[key] = int(values[key])

                self.data.append(values)

    @property
    def _matching_regex(self):
        return re.compile(
            r"(?P<min>[0-9]+)-(?P<max>[0-9]+) (?P<letter>[a-z]): (?P<password>[a-z]+)"
        )

    def read_data(self):
        """
        Return yield(data)
        """
        logging.debug("PasswordDataConsumer:read_data")
        for value in self.data:
            yield value


class PasswordTester(PasswordDataConsumer):
    def find_valid_passwords(self):
        valid_passwords = []
        for password_data in self.read_data():
            if self.is_valid(password_data):
                valid_passwords.append(password_data["password"])

        return valid_passwords

    @staticmethod
    def is_valid(data):
        occurences = len([x for x in data["password"] if x == data["letter"]])
        return data["min"] <= occurences and data["max"] >= occurences


class NewPasswordTester(PasswordTester):
    match_to_int = ["pos1", "pos2"]

    @property
    def _matching_regex(self):
        return re.compile(
            r"(?P<pos1>[0-9]+)-(?P<pos2>[0-9]+) (?P<letter>[a-z]): (?P<password>[a-z]+)"
        )

    @staticmethod
    def is_valid(data):
        logging.debug(f"NewPasswordTester:is_valid {data}")
        test = (
            data["password"][data["pos1"] - 1] == data["letter"]
            or data["password"][data["pos2"] - 1] == data["letter"]
        ) and data["password"][data["pos1"] - 1] != data["password"][data["pos2"] - 1]
        logging.debug(f"{test}")
        return test


class PasswordTesterTestCase(unittest.TestCase):
    def test_example_part_1(self):
        pt = PasswordTester("data/test-data-day-2")
        assert pt.find_valid_passwords() == ["abcde", "ccccccccc"]

    def test_example_part_2(self):
        npt = NewPasswordTester("data/test-data-day-2")
        assert npt.find_valid_passwords() == ["abcde"]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")
        pt = PasswordTester("data/input-day-2")
        result = pt.find_valid_passwords()

        logging.info(f"Part 1 number of results found: {len(result)}")

        npt = NewPasswordTester("data/input-day-2")
        result = npt.find_valid_passwords()

        logging.info(f"Part 2 number of results found: {len(result)}")

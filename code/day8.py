# -*- coding: utf-8 -*-
"""Advent of code 2020, day 8

Usage:
   day8.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class EndReached(Exception):
    pass


class LoopError(Exception):
    pass


class NotFound(Exception):
    pass


class GameConsole:
    def __init__(self, filename):
        logging.debug(f"GameConsole:init:{filename}")

        self.reset_computer()
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")

    def reset_computer(self):
        self.current_instruction = 0
        self.accumulator = 0
        self.already_executed = []

    @property
    def instructions(self):
        return [self._format_item(x) for x in self.data]

    @staticmethod
    def _format_item(item):
        code, arg = item.split()
        return {
            "code": code,
            "arg": arg,
        }

    def run_code(self):
        self.run_specific_code(self.instructions)

    def run_specific_code(self, instructions):
        def instruction_nop(arg):
            self.current_instruction += 1

        def instruction_acc(arg):
            self.accumulator += int(arg)
            self.current_instruction += 1

        def instruction_jmp(arg):
            self.current_instruction += int(arg)

        code = {
            "nop": instruction_nop,
            "acc": instruction_acc,
            "jmp": instruction_jmp,
        }
        self.reset_computer()
        while True:
            if self.current_instruction in self.already_executed:
                raise LoopError(self.accumulator)

            if self.current_instruction > len(instructions) - 1:
                raise EndReached(self.accumulator)

            self.already_executed.append(self.current_instruction)
            code[instructions[self.current_instruction]["code"]](
                instructions[self.current_instruction]["arg"]
            )

    def modify_code_and_run(self):
        change_index = 0
        while True:
            change_index = self.find_next_jmp_or_nop(index=change_index)
            modified_code = self.get_modified_code(index=change_index)
            logging.debug(f"change_index: {change_index} / {modified_code}")
            try:
                self.run_specific_code(modified_code)
            except LoopError:
                change_index += 1
                continue

    def find_next_jmp_or_nop(self, index):
        code = [x["code"] for x in self.instructions]
        try:
            jmp = code.index("jmp", index)
        except ValueError:
            jmp = None

        try:
            nop = code.index("nop", index)
        except ValueError:
            if jmp is None:
                raise NotFound

            return jmp

        if jmp is None:
            return nop

        return min(
            nop,
            jmp,
        )

    def get_modified_code(self, index):
        new_instructions = self.instructions[:]
        if new_instructions[index]["code"] == "nop":
            new_instructions[index]["code"] = "jmp"

        elif new_instructions[index]["code"] == "jmp":
            new_instructions[index]["code"] = "nop"

        return new_instructions


class GameConsoleTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = GameConsole("data/test-data-day-8")
        with self.assertRaises(LoopError) as context:  # NOQA: PT009
            runner.run_code()
            assert int(str(context.exception)) == 5

    def test_example_part_2(self):
        runner = GameConsole("data/test-data-day-8")
        with self.assertRaises(EndReached) as context:  # NOQA: PT009
            runner.modify_code_and_run()
            assert int(str(context.exception)) == 8


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = GameConsole("data/input-day-8")
        try:
            runner.run_code()
        except LoopError as e:
            accumulator = int(str(e))
            logging.info(f"Part 1 result {accumulator}")

        logging.info("Part 2")
        runner = GameConsole("data/input-day-8")
        try:
            runner.modify_code_and_run()
        except EndReached as e:
            accumulator = int(str(e))
            logging.info(f"Part 2 result {accumulator}")

# -*- coding: utf-8 -*-
"""Advent of code 2020, day 3

Usage:
   day3.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


PART_2_DIRECTIONS = [
    {
        "inc_x": 1,
        "inc_y": 1,
        "test_result": 2,
    },
    {
        "inc_x": 3,
        "inc_y": 1,
        "test_result": 7,
    },
    {
        "inc_x": 5,
        "inc_y": 1,
        "test_result": 3,
    },
    {
        "inc_x": 7,
        "inc_y": 1,
        "test_result": 4,
    },
    {
        "inc_x": 1,
        "inc_y": 2,
        "test_result": 2,
    },
]


class OutOfMap(Exception):
    pass


class Map:
    def __init__(self, filename):
        logging.debug(f"Map:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")
            self.map_width = len(self.data[0])

    def is_tree_at(self, x, y):
        """
        Return true if tree is present at given point
        Raise OutOfMap if y > map
        """
        try:
            if self.data[y][x % self.map_width] == "#":
                logging.debug(f"Map:is_tree_at x:{x}, y:{y} => TREE")
                return True
            else:
                logging.debug(f"Map:is_tree_at x:{x}, y:{y} => empty")
                return False
        except IndexError as e:
            logging.debug(f"Map:is_tree_at x:{x}, y:{y} => OutOfMap")
            raise OutOfMap(e)


class TreesCounter:
    def __init__(self, filename):
        self.map = Map(filename)
        self.x, self.y = 0, 0

    def move(self):
        self.x += 3
        self.y += 1

    def travel_and_count_trees(self):
        nb_trees = 0
        while True:
            try:
                if self.map.is_tree_at(self.x, self.y):
                    nb_trees += 1

                self.move()
            except OutOfMap:
                return nb_trees


class TreesCounterSpecificPath(TreesCounter):
    def __init__(self, filename, inc_x=3, inc_y=1):
        super().__init__(filename)
        self.inc_x = inc_x
        self.inc_y = inc_y

    def move(self):
        self.x += self.inc_x
        self.y += self.inc_y


class TreesCounterTestCase(unittest.TestCase):
    def test_example_part_1(self):
        tc = TreesCounter("data/test-data-day-3")
        assert tc.travel_and_count_trees() == 7

    def test_example_part_2(self):
        for values in PART_2_DIRECTIONS:
            tcsp = TreesCounterSpecificPath(
                "data/test-data-day-3", inc_x=values["inc_x"], inc_y=values["inc_y"]
            )
            assert tcsp.travel_and_count_trees() == values["test_result"]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        tc = TreesCounter("data/input-day-3")
        result = tc.travel_and_count_trees()
        logging.info(f"Part 1 number of trees found: {result}")

        answer = 1
        for values in PART_2_DIRECTIONS:
            tcsp = TreesCounterSpecificPath(
                "data/input-day-3", inc_x=values["inc_x"], inc_y=values["inc_y"]
            )
            result = tcsp.travel_and_count_trees()
            logging.info(
                f"Part 2 number of trees found: {result} for x:{values['inc_x']}, y:{values['inc_y']}"
            )
            answer = answer * result

        logging.info(f"Part 2 answer: {answer}")

# -*- coding: utf-8 -*-
"""Advent of code 2020, day 7

Usage:
   day7.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class BagRules:
    def __init__(self, filename):
        logging.debug(f"BagRules:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = {}
            for line in data_file.read().strip().split("\n"):
                rule = self._format_rule(line)
                self.data[rule["bag"]] = rule["contains"]

    @staticmethod
    def _format_rule(item):
        container, content = item.split(" bags contain ")
        logging.debug(f"BagRules:_format_item: container:{container} content:{content}")
        inside_bags = []
        if "no other bags" not in content:
            for bag in content.split(","):
                logging.debug(f"BagRules:_format_item: bag:{bag}")
                inside_bags.append(
                    {
                        "number": int(bag.strip().split(" ")[0].strip()),
                        "color": " ".join(bag.strip().split(" ")[1:3])
                        .strip()
                        .replace(".", ""),
                    }
                )
        rule = {
            "bag": container,
            "contains": inside_bags,
        }
        logging.debug(f"BagRules:_format_item: rule:{rule}")
        return rule

    def can_contains(self, bag):
        bags = []
        for container_bag in self.data:
            if self.bag_can_contains(container_bag, bag):
                bags.append(container_bag)

        return bags

    def bag_can_contains(self, container_bag, bag):
        bag_definition = self.data[container_bag]
        if bag in (x["color"] for x in bag_definition):
            return True

        for inside_bag in bag_definition:
            if self.bag_can_contains(container_bag=inside_bag["color"], bag=bag):
                return True

        return False

    def must_contains(self, bag, first_call=True):
        bag_definition = self.data[bag]
        nb = 0 if first_call else 1

        for contained_bag in bag_definition:
            contained_bags = (
                self.must_contains(contained_bag["color"], first_call=False)
                * contained_bag["number"]
            )
            nb += contained_bags

        return nb


class BagRulesTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = BagRules("data/test-data-day-7")
        nb_of_bags = len(list(runner.can_contains("shiny gold")))
        assert nb_of_bags == 4

    def test_example_part_2(self):
        runner = BagRules("data/test-data-day-7-part-2")
        nb_of_bags = runner.must_contains("shiny gold")
        print(nb_of_bags)
        assert nb_of_bags == 126


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = BagRules("data/input-day-7")
        nb_of_bags = len(list(runner.can_contains("shiny gold")))
        logging.info(f"Part 1 : {nb_of_bags}")

        logging.info("Part 2")
        runner = BagRules("data/input-day-7")
        nb_of_bags = runner.must_contains("shiny gold")
        logging.info(f"Part 2 : {nb_of_bags}")

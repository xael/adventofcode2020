# -*- coding: utf-8 -*-
"""Advent of code 2020, day 9

Usage:
   day9.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class EncodinError:
    def __init__(self, filename, preamble_length=25):
        logging.debug(f"EncodinError:init:{filename}")
        self.preamble_length = preamble_length
        with open(filename, "r") as data_file:
            self.data = [
                self._format_item(x) for x in data_file.read().strip().split("\n")
            ]

    @staticmethod
    def _format_item(item):
        return int(item)

    def find_invalid_number(self):
        index = self.preamble_length
        while True:
            items_before_index = self.get_items_before_index(index)
            value = self.data[index]
            if not self.is_valid_number(value, items_before_index):
                return value
            index += 1

    def get_items_before_index(self, index):
        return self.data[index - 25 : index]

    @classmethod
    def is_valid_number(cls, value, items_before_index):
        for i in range(len(items_before_index)):
            tested_value = items_before_index[i]
            complement = value - tested_value
            if complement in items_before_index[:i] + items_before_index[i + 1 :]:
                return True

        return False

    def find_continuous_numbers_which_sum_as(self, sum_as):
        for i in range(len(self.data)):
            current_sum = self.data[i]
            nb_of_match = 0
            for j in range(i + 1, len(self.data)):
                current_sum += self.data[j]
                nb_of_match += 1
                if current_sum == sum_as and nb_of_match >= 2:
                    return self.data[i : j + 1]
                elif current_sum > sum_as:
                    break


class EncodinErrorTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = EncodinError("data/test-data-day-9", preamble_length=5)
        number = runner.find_invalid_number()
        assert number == 127

    def test_example_part_2(self):
        runner = EncodinError("data/test-data-day-9")
        list_of_numbers = runner.find_continuous_numbers_which_sum_as(127)
        assert list_of_numbers == [15, 25, 47, 40]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = EncodinError("data/input-day-9")
        number = runner.find_invalid_number()
        logging.info(f"Part 1 : {number}")

        logging.info("Part 2")
        runner = EncodinError("data/input-day-9")
        list_of_numbers = runner.find_continuous_numbers_which_sum_as(number)
        sum_of_min_max = min(list_of_numbers) + max(list_of_numbers)
        logging.info(f"Part 1 : {sum_of_min_max}")

# -*- coding: utf-8 -*-
"""Advent of code 2020, day 14

Usage:
   day14.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from itertools import accumulate

from docopt import docopt


MASK = "mask"
MEM = "mem"


class DockingData:
    def __init__(self, filename):
        logging.debug(f"DockingData:init:{filename}")
        self._reset()
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")

    @property
    def items(self):
        for item in self.data:
            yield self._format_item(item)

    @staticmethod
    def _format_item(item):
        return [x.strip() for x in item.split("=")]

    def _reset(self):
        self.mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        self.memory = {}

    def start(self):
        for code, value in self.items:
            if code == MASK:
                self.mask = value

            if code.startswith(MEM):
                addr = int(code.split("[")[1].replace("]", ""))
                value = int(value)
                self._apply_mask_and_set_memory(addr, value)

        logging.debug(self.memory)
        memory_sum = list(accumulate(self.memory.values()))[-1]
        return memory_sum

    def _apply_mask_and_set_memory(self, addr, value):
        value = self._apply_mask(self.mask, value)
        self.memory[addr] = value

    @staticmethod
    def _apply_mask(mask, value):
        logging.debug(f"_apply_mask: {mask} @{value}")
        mask_or = int(mask.replace("X", "0"), 2)
        mask_and = int(mask.replace("X", "1"), 2)
        value = value & mask_and
        logging.debug(f"_apply_mask: after and {value}")
        value = value | mask_or
        logging.debug(f"_apply_mask: after or {value}")
        return value


class DockingDataV2(DockingData):
    def _apply_mask_and_set_memory(self, addr, value):
        addresses = self._get_adresses(addr)
        for address_mask in addresses:
            mask = int(self.mask.replace("0", "1").replace("X", "0"), 2)
            a = addr & mask
            a = a | address_mask
            logging.info(f"write {value} @{a}")
            self.memory[a] = value

        logging.debug(self.memory)
        memory_sum = list(accumulate(self.memory.values()))[-1]
        return memory_sum

    def _get_adresses(self, addr):
        logging.debug(f"_get_addresses: {self.mask} @{addr}")
        addr = self._apply_mask(self.mask, addr)
        mask_or = int(self.mask.replace("X", "0"), 2)
        addr = addr | mask_or

        addresses = self._calculate_addresses_by_changing_bit(self.mask)
        logging.debug(f"_get_addresses: {addresses}")
        return addresses

    @staticmethod
    def _calculate_addresses_by_changing_bit(addr):
        if "X" in addr:
            index = addr.index("X")
            addr_0 = addr[:index] + "0" + addr[index + 1 :]
            addr_1 = addr[:index] + "1" + addr[index + 1 :]
            values = DockingDataV2._calculate_addresses_by_changing_bit(
                addr_0
            ) + DockingDataV2._calculate_addresses_by_changing_bit(addr_1)
            return values
        else:
            values = [int(addr, 2)]
            return values

    @staticmethod
    def _apply_mask(mask, value):
        mask_or = int(mask.replace("X", "0"), 2)
        value = value | mask_or
        return value


class DockingDataTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = DockingData("data/test-data-day-14")
        memory_sum = runner.start()
        logging.info(f"Test Part 1: {memory_sum}")
        assert memory_sum == 165

    def test_example_part_2(self):
        runner = DockingDataV2("data/test-data-day-14-part-2")
        memory_sum = runner.start()
        logging.info(f"Test Part 2: {memory_sum}")
        assert memory_sum == 208


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = DockingData("data/input-day-14")
        memory_sum = runner.start()
        logging.info(f"Part 1: {memory_sum}")

        logging.info("Part 2")
        runner = DockingDataV2("data/input-day-14")
        memory_sum = runner.start()
        logging.info(f"Part 2: {memory_sum}")

# -*- coding: utf-8 -*-
"""Advent of code 2020, day 1

Usage:
   day1.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import random
import sys
import unittest

from docopt import docopt


class DataConsumer:
    def __init__(self, filename):
        logging.debug(f"DataConsumer:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = [int(x) for x in data_file.read().strip().split("\n")]

    def read_data(self, tested_values=None):
        """
        Return yield(data) and assume that tested_values are not in returned values
        """
        logging.debug(f"DataConsumer:read_data:exclude {tested_values}")
        tested_values = tested_values if tested_values else []
        for value in [x for x in self.data if x not in tested_values]:
            yield value


class ExpenseReport(DataConsumer):
    def fix_expense_report(self):
        numbers = list(self.read_data())
        nb = len(numbers)
        logging.debug(f"Test {nb} values")
        while True:
            nb1 = random.randint(0, nb - 1)
            nb2 = random.randint(0, nb - 1)
            logging.debug(f"Test {numbers[nb1]}@{nb1} + {numbers[nb2]}@{nb2}")
            if nb1 == nb2:
                continue
            if numbers[nb1] + numbers[nb2] == 2020:
                break
        return (numbers[nb1], numbers[nb2])


class ExpenseReportThree(ExpenseReport):
    def fix_expense_report(self):
        numbers = list(self.read_data())
        nb = len(numbers)
        logging.debug(f"Test {nb} values")
        while True:
            nb1 = random.randint(0, nb - 1)
            nb2 = random.randint(0, nb - 1)
            nb3 = random.randint(0, nb - 1)
            logging.debug(
                f"Test {numbers[nb1]}@{nb1} + {numbers[nb2]}@{nb2} + {numbers[nb3]}@{nb3}"
            )
            if nb1 == nb2 or nb1 == nb3 or nb2 == nb3:
                continue
            if numbers[nb1] + numbers[nb2] + numbers[nb3] == 2020:
                break
        return (numbers[nb1], numbers[nb2], numbers[nb3])


class ExpenseReportTestCase(unittest.TestCase):
    def test_example_part_1(self):
        er = ExpenseReport("data/test-data-day-1")
        result = er.fix_expense_report()
        assert sorted(result) == [299, 1721]

    def test_example_part_2(self):
        er = ExpenseReportThree("data/test-data-day-1")
        result = er.fix_expense_report()
        assert sorted(result) == [366, 675, 979]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        er = ExpenseReport("data/input-day-1")
        result = er.fix_expense_report()

        logging.info(f"Part 1 result found: {result} => {result[0] * result[1]}")

        ert = ExpenseReportThree("data/input-day-1")
        result = ert.fix_expense_report()

        logging.info(
            f"Part 2 result found: {result} => {result[0] * result[1] * result[2]}"
        )

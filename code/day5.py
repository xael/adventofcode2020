# -*- coding: utf-8 -*-
"""Advent of code 2020, day 5

Usage:
   day5.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class BinaryBoarding:
    def __init__(self, filename):
        logging.debug(f"BinaryBoarding:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")
        logging.debug(f"BinaryBoarding:init:got {len(self.data)} tickets")

    @property
    def seats_id(self):
        for ticket in self.data:
            yield self.convert_ticket_to_seat_id(ticket)

    @staticmethod
    def convert_ticket_to_seat_id(ticket):
        binary_format = (
            ticket.replace("B", "1")
            .replace("F", "0")
            .replace("R", "1")
            .replace("L", "0")
        )
        seat_id = int(binary_format, 2)
        return seat_id

    def find_missing_seat_id(self):
        tickets_id = list(self.seats_id)
        min_id = min(tickets_id)
        max_id = max(tickets_id)
        logging.debug(f"min: {min_id} / max: {max_id}")
        logging.debug(f"tickets: {tickets_id}")
        return [x for x in range(min_id, max_id) if x not in tickets_id]


class BinaryBoardingTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = BinaryBoarding("data/test-data-day-5")
        assert list(runner.seats_id) == [567, 119, 820]


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = BinaryBoarding("data/input-day-5")
        max_id = max(runner.seats_id)
        logging.info(f"Max ID found {max_id}")

        logging.info("Part 2")
        missing_id = runner.find_missing_seat_id()
        logging.info(f"Missing ID found {missing_id}")

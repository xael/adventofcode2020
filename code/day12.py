# -*- coding: utf-8 -*-
"""Advent of code 2020, day 12

Usage:
   day12.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import math
import sys
import unittest

from docopt import docopt


class RainRisk:
    ORIENTATION_TABLE = {
        "N": {"n": 1, "e": 0, "cap": 0, "dcap": 0},
        "E": {"n": 0, "e": 1, "cap": 90, "dcap": 0},
        "S": {"n": -1, "e": 0, "cap": 180, "dcap": 0},
        "W": {"n": 0, "e": -1, "cap": 270, "dcap": 0},
        "L": {"n": 0, "e": 0, "cap": 0, "dcap": -1},
        "R": {"n": 0, "e": 0, "cap": 0, "dcap": 1},
    }

    def __init__(self, filename):
        logging.debug(f"RainRisk:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")
            self.orientation = self._convert_orientation("E")["cap"]
            self.n = 0
            self.e = 0

    @staticmethod
    def _convert_orientation(orientation):
        return RainRisk.ORIENTATION_TABLE[orientation]

    @staticmethod
    def _get_direction_for_cap(v):
        logging.debug(f"_get_direction_for_cap: {v}")
        for item in RainRisk.ORIENTATION_TABLE:
            if RainRisk.ORIENTATION_TABLE[item]["cap"] == v:
                logging.debug(f"{RainRisk.ORIENTATION_TABLE[item]}")
                return RainRisk.ORIENTATION_TABLE[item]

    @property
    def items(self):
        for item in self.data:
            yield self._format_item(item)

    @staticmethod
    def _format_item(item):
        return (item[0], int(item[1:]))

    def navigate(self):
        for instruction in self.items:
            cap, length = instruction
            if cap in "NESWLR":
                self.n += self._convert_orientation(cap)["n"] * length
                self.e += self._convert_orientation(cap)["e"] * length
                self.orientation = (
                    self.orientation + self._convert_orientation(cap)["dcap"] * length
                ) % 360
            elif cap in "F":
                direction = self._get_direction_for_cap(self.orientation)
                self.n += direction["n"] * length
                self.e += direction["e"] * length

            logging.debug(
                f"instruction: {instruction}, n:{self.n}, e:{self.e}, orientation:{self.orientation}"
            )

        return self._convert_coord(self.n, self.e)

    @staticmethod
    def _convert_coord(n, e):
        return abs(n) + abs(e)


class RainRiskWaypoint(RainRisk):
    def __init__(self, filename):
        super().__init__(filename)
        self.waypoint_n = 1
        self.waypoint_e = 10

    def _rotate_waypoint(self, angle):
        self.waypoint_n, self.waypoint_e = (
            self.waypoint_n * round(math.cos(math.radians(angle)))
            - self.waypoint_e * round(math.sin(math.radians(angle))),
            self.waypoint_n * round(math.sin(math.radians(angle)))
            + self.waypoint_e * round(math.cos(math.radians(angle))),
        )

    def navigate(self):
        for instruction in self.items:
            cap, length = instruction
            if cap in "NESW":
                self.waypoint_n += self._convert_orientation(cap)["n"] * length
                self.waypoint_e += self._convert_orientation(cap)["e"] * length
            elif cap in "RL":
                angle = length * (cap == "R" or -1)
                self._rotate_waypoint(angle)

            elif cap in "F":
                self.n += self.waypoint_n * length
                self.e += self.waypoint_e * length

            logging.debug(
                f"instruction: {instruction}, n:{self.n}, e:{self.e}, wp_n:{self.waypoint_n}, wp_e:{self.waypoint_e}"
            )

        return self._convert_coord(self.n, self.e)


class RainRiskTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = RainRisk("data/test-data-day-12")
        position = runner.navigate()
        logging.info(f"Test Part 1: {position}")
        assert position == 25

    def test_example_part_2(self):
        runner = RainRiskWaypoint("data/test-data-day-12")
        position = runner.navigate()
        logging.info(f"Test Part 2: {position}")
        assert position == 286


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = RainRisk("data/input-day-12")
        position = runner.navigate()
        logging.info(f"Part 1: {position}")

        logging.info("Part 2")
        runner = RainRiskWaypoint("data/input-day-12")
        position = runner.navigate()
        logging.info(f"Part 2: {position}")

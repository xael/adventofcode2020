# -*- coding: utf-8 -*-
"""Advent of code 2020, day 15

Usage:
   day15.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class RambunctiousRecitation:
    def __init__(self, start_of_game):
        self.spoken_numbers = {}
        self.spoken_numbers_round_before = {}
        self.move_count = 0
        self.last_number = None
        self.before_last_number = None
        for n in start_of_game:
            self.add_number(n)

    def add_number(self, number):
        self.spoken_numbers_round_before[self.last_number] = self.move_count - 1
        self.spoken_numbers[number] = self.move_count
        self.last_number = number
        self.move_count += 1

    def play_until(self, moves=2020):
        while moves != self.move_count:
            self.play_next_number()

        return self.last_number

    def number_rank(self, number):
        try:
            return self.move_count - self.spoken_numbers_round_before[number] - 1
        except KeyError:
            return None

    def play_next_number(self):
        last = self.last_number
        nr = self.number_rank(last)

        if nr is not None:
            self.add_number(nr)
        else:
            self.add_number(0)


class RambunctiousRecitationTestCase(unittest.TestCase):
    def test_example_part_1(self):
        inputs = (
            ((0, 3, 6), 0, 10),
            ((1, 3, 2), 1, 2020),
            ((2, 1, 3), 10, 2020),
            ((1, 2, 3), 27, 2020),
            ((2, 3, 1), 78, 2020),
            ((3, 2, 1), 438, 2020),
            ((3, 1, 2), 1836, 2020),
        )
        for inp, out, moves in inputs:
            runner = RambunctiousRecitation(inp)
            result = runner.play_until(moves=moves)
            assert result == out


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = RambunctiousRecitation((5, 2, 8, 16, 18, 0, 1))
        result = runner.play_until(moves=2020)
        logging.info(f"Part 1: {result}")
        print(runner.spoken_numbers)

        logging.info("Part 2")
        runner = RambunctiousRecitation((5, 2, 8, 16, 18, 0, 1))
        result = runner.play_until(moves=30000000)
        logging.info(f"Part 2: {result}")

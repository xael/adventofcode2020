# -*- coding: utf-8 -*-
"""Advent of code 2020, day 1

Usage:
   day1.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


class DataConsumer:
    def __init__(self, filename):
        logging.debug(f"DataConsumer:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = [int(x) for x in data_file.read().strip().split("\n")]

    def read_data(self, tested_values=None):
        """
        Return yield(data) and assume that tested_values are not in returned values
        """
        logging.debug(f"DataConsumer:read_data:exclude {tested_values}")
        tested_values = tested_values if tested_values else []
        for value in [x for x in self.data if x not in tested_values]:
            yield value


class ExpenseReport(DataConsumer):
    def find_complement_for_target(self, value, target=2020, tested_values=None):
        tested_values = tested_values if tested_values else [value]
        complement = target - value
        if complement in self.read_data(tested_values=[value]):
            logging.debug(f"Found complement {complement} for value {value}")
            return complement

        return None

    def fix_expense_report(self):
        for value in self.read_data():
            logging.debug(f"try value {value}")
            matching_expense = self.find_complement_for_target(value)
            if matching_expense:
                return (value, matching_expense)


class ExpenseReportThree(ExpenseReport):
    def fix_expense_report(self):
        for value1 in self.read_data():
            for value2 in self.read_data(tested_values=[value1]):
                logging.debug(f"try values {value1}, {value2}")
                matching_expense = self.find_complement_for_target(
                    value=value2, target=2020 - value1, tested_values=[value1, value2]
                )
                if matching_expense:
                    return (value1, value2, matching_expense)


class ExpenseReportTestCase(unittest.TestCase):
    def test_example_part_1(self):
        er = ExpenseReport("data/test-data-day-1")
        assert er.fix_expense_report() == (1721, 299)

    def test_example_part_2(self):
        er = ExpenseReportThree("data/test-data-day-1")
        assert er.fix_expense_report() == (979, 366, 675)


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        er = ExpenseReport("data/input-day-1")
        result = er.fix_expense_report()

        logging.info(f"Part 1 result found: {result} => {result[0] * result[1]}")

        ert = ExpenseReportThree("data/input-day-1")
        result = ert.fix_expense_report()

        logging.info(
            f"Part 2 result found: {result} => {result[0] * result[1] * result[2]}"
        )

build:
	docker build -f docker/Dockerfile -t adventofcode .

requirements:
	pip-compile requirements.in
	pip-compile requirements-dev.in
